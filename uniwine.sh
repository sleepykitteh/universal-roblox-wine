#!/bin/bash
echo "Script started..."

# make the directories that the script will soon be working in
echo "Making temporary directories..."
mkdir /tmp/sleepykitteh/grapejuicewine
cd /tmp/sleepykitteh/grapejuicewine/

# download wine from the Grapejuice Enthusiasts discord server's totally-not-a-fileshare channel
echo "Grabbing wine from Grapejuice Enthusiasts..."
wget https://cdn.discordapp.com/attachments/858117357897121822/943053350528778240/debuntu-wine-tkg-staging-fsync-git-7.2.r0.g68441b1d.7z

# extract the wine 7z to the directory where i put the thinghy
echo "Extracting the wine 7z that just got downloaded..."
p7zip -d debuntu-wine-tkg-staging-fsync-git-7.2.r0.g68441b1d.7z /tmp/sleepykitteh/grapejuicewine/usr/

# THIS PART IS VERY, VERY DANGEROUS!
# ANYTHING INSIDE THE "/tmp/sleepykitteh/grapejuicewine/usr/" DIRECTORY WILL BE COPIED TO ROOT!
echo "Using root to copy the files lmao"
cp /tmp/sleepykitteh/grapejuicewine/wine-tkg-staging-fsync-git-7.2.r0.g68441b1d/ /usr
echo "Done! You should now have wine installed. Going to run wine --version now!"
wine --version
echo "You should see the wine version above, if not, make an issue on the Gitea page!"
echo "sleepykitteh wuz here lulz"
